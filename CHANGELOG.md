## [1.0.4](https://gitlab.com/6clc/semantic-release/compare/v1.0.3...v1.0.4) (2020-12-18)


### Bug Fixes

* adf ([9d79475](https://gitlab.com/6clc/semantic-release/commit/9d79475a55aa5fdf448016d4d062e559516a8362))
* adf ([21acde4](https://gitlab.com/6clc/semantic-release/commit/21acde4aa7158dbf8c4b60767b7639b73438bb7d))
* adsf ([617d170](https://gitlab.com/6clc/semantic-release/commit/617d170f2918c9d73b5f29f40a04cec8a436dd9e))
* fix bug ([ce31a99](https://gitlab.com/6clc/semantic-release/commit/ce31a99880388a9cac20a9c2fd57645b4ca92dbb)), closes [#2](https://gitlab.com/6clc/semantic-release/issues/2)
* gitlab script error ? ([b3c695f](https://gitlab.com/6clc/semantic-release/commit/b3c695fa1ee1232e2268065060638613f92074cf))

## [1.0.3](https://gitlab.com/6clc/semantic-release/compare/v1.0.2...v1.0.3) (2020-12-18)


### Bug Fixes

* fix 1 issuse ([0920a1f](https://gitlab.com/6clc/semantic-release/commit/0920a1f0fc0cc2c00416568d06230af7581e593e)), closes [#1](https://gitlab.com/6clc/semantic-release/issues/1)

## [1.0.2](https://gitlab.com/6clc/semantic-release/compare/v1.0.1...v1.0.2) (2020-12-18)


### Bug Fixes

* issue? ([5aef5c0](https://gitlab.com/6clc/semantic-release/commit/5aef5c0949781b36a5876e8d57ce085c2d2bebf9)), closes [#123](https://gitlab.com/6clc/semantic-release/issues/123)

## [1.0.1](https://gitlab.com/6clc/semantic-release/compare/v1.0.0...v1.0.1) (2020-12-18)


### Bug Fixes

* gitlab version fix ([d286ba2](https://gitlab.com/6clc/semantic-release/commit/d286ba21847a9be8d6d55f89d727297ab01a5672))

# 1.0.0 (2020-12-18)


### Features

* init ([f47137a](https://gitlab.com/6clc/semantic-release/commit/f47137a36bcfe718fa2d74752f809b6a19273868))
* init ([3fbd263](https://gitlab.com/6clc/semantic-release/commit/3fbd263e4d11f12a2b8ec5059ea6f9d70a606f20))
